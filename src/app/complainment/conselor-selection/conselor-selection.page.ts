import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { environment } from 'src/environments/environment'
import { ProvidersService } from 'src/app/services/providers.service';
const ENV = environment.apiUrl;
@Component({
  selector: 'app-conselor-selection',
  templateUrl: './conselor-selection.page.html',
  styleUrls: ['./conselor-selection.page.scss'],
})
export class ConselorSelectionPage implements OnInit {

  @Input() category_id: string;
  conselors = []
  constructor(private modalCtrl: ModalController, private api: ProvidersService) { }

  ngOnInit() {
    this.api.getConselorByCategory(this.category_id).subscribe((res: any) => {
      this.conselors = res
      console.log(this.conselors)
    })
  }

  closeModal() {
    this.modalCtrl.dismiss(false, "false")
  }

  selected(conselor) {
    console.log(conselor)
    this.modalCtrl.dismiss(conselor, "true")
  }
}
