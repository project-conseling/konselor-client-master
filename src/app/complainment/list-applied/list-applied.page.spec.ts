import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAppliedPage } from './list-applied.page';

describe('ListAppliedPage', () => {
  let component: ListAppliedPage;
  let fixture: ComponentFixture<ListAppliedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListAppliedPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListAppliedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
