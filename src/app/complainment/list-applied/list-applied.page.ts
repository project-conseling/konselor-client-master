import { Component, OnInit } from '@angular/core';
import { ProvidersService } from 'src/app/services/providers.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Socket } from 'ng-socket-io';
import { ModalController } from '@ionic/angular';
import { DetailComplaintPage } from '../detail-complaint/detail-complaint.page';

@Component({
  selector: 'app-list-applied',
  templateUrl: './list-applied.page.html',
  styleUrls: ['./list-applied.page.scss'],
})
export class ListAppliedPage implements OnInit {
  segm: string = "pengaduan"
  complaintApp = []
  approvedTemp = []
  approved = []
  stayed = []
  categories = [];
  onload: boolean = false

  constructor(public api: ProvidersService, private socket: Socket, private router: Router,  private modalCtrl: ModalController) { }
  async ngOnInit() {
    this.getUpdateComplaint()
            .subscribe(resp => {
              console.log("ada update")
            })
    this.complaintApp = [];
    try {
      this.onload = true
      await this.api.getCategories()
      .subscribe(async (resCat: any) => {
        console.log('resp', resCat)
        this.categories = resCat;
        this.api.getComplain()
        .subscribe(async (res: any) => {
          console.log("respon", res)
          
          this.complaintApp = await res
          this.complaintApp.forEach(complaint => {
            if(complaint.status == 9) {
              
              this.approved.push(complaint)
            } else {
              this.stayed.push(complaint)
            }
          })
          
  
          // this.getUpdate()
          //   .subscribe(update => {
          //     this.updateData()
          //   })
        })
      })
      
    } catch (error) {
      console.log(error)
    } finally {
      this.segm = "pengaduan"
      setTimeout(() => {
        this.onload = false
      }, 2000)
    } 

    
    
  }

  getUpdateComplaint() {
    let observable = new Observable(obs => {
      this.socket.on('update-complaint', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }

  segmChanged($event) {
    this.segm = $event.detail.value
  }

  getUpdate(){
    let observable = new Observable(obs => {
      this.socket.on('patient-status-changed', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }

  updateData(){
    this.complaintApp = [];
    this.api.getComplain()
    .subscribe((res: any) => {
      res.data.forEach(element => {
          this.api.getProfile(element.patientId)
          .subscribe((profile: any) => {
            element.profile = profile.data[0];
          })
          this.complaintApp.push(element)
      });
      console.log(this.complaintApp)
    })
  } 

  onKey($event) {
    const temp = this.approved
    this.approved = []
    setTimeout(() => {
      this.approved = temp
    }, 3000)
    // if($event.key == '') {
    //   this.approved = this.approvedTemp
    // } else {
    //   this.approvedTemp = this.approved
    //   let filter = this.approved.filter(function(item){
    //     console.log(item)
    //       return typeof item.patient.profile == 'string' && item.patient.profile.indexOf($event.key) > -1;            
    //   });
    //   console.log(filter)
    // }
  }

  getText(id) {
    let text;
    console.log('ini id',id)
    // console.log(this.categories)
    this.categories.forEach(category => {
      if(category.id == id) {
        text = category.category
      }
    })
    console.log(text)
    return text;
  }

  normalizeDate(dateChip) {
    let date = dateChip
    let dateChiper = date.split("T")
    let dateArr = dateChiper[0].split("-");
    let dateText;
    switch (dateArr[1]) {
      case "01":
        dateText = dateArr[2]+" Januari "+dateArr[0]
        break;
      case "02":
        dateText = dateArr[2]+" Februari "+dateArr[0]
        break;
      case "03":
        dateText = dateArr[2]+" Maret "+dateArr[0]
        break;
      case "04":
        dateText = dateArr[2]+" April "+dateArr[0]
        break;
      case "05":
        dateText = dateArr[2]+" Mei "+dateArr[0]
        break;
      case "06":
        dateText = dateArr[2]+" Juni "+dateArr[0]
        break;
      case "07":
        dateText = dateArr[2]+" Juli "+dateArr[0]
        break;
      case "08":
        dateText = dateArr[2]+" Agustus "+dateArr[0]
        break;
      case "09":
        dateText = dateArr[2]+" September "+dateArr[0]
        break;
      case "10":
        dateText = dateArr[2]+" Oktober "+dateArr[0]
        break;
      case "11":
        dateText = dateArr[2]+" November "+dateArr[0]
        break;
      case "12":
        dateText = dateArr[2]+" Desember "+dateArr[0]
        break;
      default:
        break;
    }
    return dateText
  }

  async normalizeTime(dateChip) {
    let date = dateChip ? dateChip : 'T'

    let dateChiper = date.split("T")
    let dateArr = dateChiper[1].split(":");
    let dateText = dateArr[0]+":"+dateArr[1];
    return dateText
  }

  async openDetail(data) {
    const modal = await this.modalCtrl.create({
      component: DetailComplaintPage,
      componentProps: {
        dataComplaint: data
      }
    });
    return await modal.present();
  }

}
