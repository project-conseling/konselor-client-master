import { Component, ViewChild } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Socket } from 'ng-socket-io';
import { ResetPasswordPage } from './user/reset-password/reset-password.page';

import { Deeplinks } from '@ionic-native/deeplinks/ngx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private socket: Socket,
    private deepLink: Deeplinks,
    private navCtrl: NavController,
    private router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();

      this.deepLink.routeWithNavController(this.navCtrl, {
        '/forget': ResetPasswordPage,
      }).subscribe((match) => {
        // match.$route - the route we matched, which is the matched entry from the arguments to route()
        // match.$args - the args passed in the link
        // match.$link - the full link data
        this.router.navigateByUrl('forget?'+match.$args.token)
        // alert("success: route => "+ match.$route +" arg => "+ JSON.stringify(match.$args) + " links => "+ JSON.stringify(match.$link))
        // console.log('Successfully matched route', match);
      },
      (nomatch) => {
        // alert("did not")
        // nomatch.$link - the full link data
        console.error('Got a deeplink that didn\'t match', nomatch);
      });
      
    });
  }
}
