import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CloudinaryService {

  constructor(private httpClient: HttpClient) { }
  cloudinaryUrl = "https://api.cloudinary.com/v1_1/conselings-media/image";
  upload(file) {
    return this.httpClient.post(`${this.cloudinaryUrl}/upload`,file);
  }
  destroy(public_id) {
    return this.httpClient.post(`${this.cloudinaryUrl}/destroy`,public_id);
  }
}
