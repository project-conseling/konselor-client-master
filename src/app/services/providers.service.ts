import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment'
import { Observable } from 'rxjs';
import { Category } from '../_types/category';
const ENV = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ProvidersService {

  constructor(private httpClient: HttpClient) { }

  getCategories() {
    return this.httpClient.get<Category>(`${ENV}/category`)
  }
  getConselorByCategory(id) {
    return this.httpClient.get(`${ENV}/specialist/conselor/${id}`)
  }

  /**Conseling API */
  getMyConseling(id) {
    return this.httpClient.get(`${ENV}/conseling/conselor/${id}`);
  }
  getOneConseling(id) {
    return this.httpClient.get(`${ENV}/conseling/consultation/${id}`);
  }
  setResult(req) {
    return this.httpClient.post(`${ENV}/conseling/result/${req._id}`, req)
  }
  

  /**Complaint API */
  getComplain() {
    return this.httpClient.get(`${ENV}/complaint`);
  }
  getComplaintId(id){
    return this.httpClient.get(`${ENV}/complaint/id/${id}`);
  }
  updateComplaint(form) {
    return this.httpClient.put(`${ENV}/complaint/${form.id}`, form)
  }
  declineComplaint(form) {
    return this.httpClient.post(`${ENV}/complaint/decline`, form)
  }
  

  /**Schedule API */
  getMyScheduleConseling(id) {
    return this.httpClient.get(`${ENV}/schedule/conselings/${id}`);
  }
  setSchedule(req) {
    return this.httpClient.post(`${ENV}/schedule`, req);
  }

  
  getMySchedule(id) {
    return this.httpClient.get(`${ENV}/schedule/me/${id}`);
  }

  getMyScheduleByDate(id, date) {
    return this.httpClient.get(`${ENV}/schedule/date/me/${id}/date/${date}`);
  }
  updateMySchedule(req) {
    return this.httpClient.put(`${ENV}/new/schedule/${req._id}`, req);
  }

  updateStatusSchedule(req) {
    return this.httpClient.put(`${ENV}/schedule/${req.conseling_id}`, req);
  }

  /**Profile API */
  getProfile(id) {
    return this.httpClient.get(`${ENV}/profile/${id}`);
  }

  /**Chat API */
  getChat(id) {
    return this.httpClient.get(`${ENV}/log/chat/${id}`);
  }
  sendChat(req) {
    return this.httpClient.post(`${ENV}/chat`, req)
  }
  readChat(id) {
    return this.httpClient.put(`${ENV}/chat/${id}`, id)
  }


  getUser(id) {
    return this.httpClient.get(`${ENV}/user/${id}`)
  }

  updateUser(form) {
    return this.httpClient.put(`${ENV}/user/update/${form._id}`, form)
  }

  // question
  getListQuestion() {
    return this.httpClient.get(`${ENV}/question`)
  }
  getOneQuestion(id) {
    return this.httpClient.get(`${ENV}/question/${id}`)
  }
  modifyQuestion(req) {
    return this.httpClient.put(`${ENV}/question/${req.id}`, req)
  }
}
