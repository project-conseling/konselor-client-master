import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/_types/category';
import { ProvidersService } from 'src/app/services/providers.service';
import { ConselingResultPage } from '../conseling-result/conseling-result.page';
import { ModalController } from '@ionic/angular';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-list-conseling',
  templateUrl: './list-conseling.page.html',
  styleUrls: ['./list-conseling.page.scss'],
})
export class ListConselingPage implements OnInit {
  onload: boolean = false;
  listConseling: any = [];
  categories: Category[];
  constructor(public api: ProvidersService, public modalCtrl: ModalController, public router: Router) { }

  async ngOnInit() {
    this.onload = true
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID;
    try {
      await this.api.getMyConseling(id)
      .subscribe( async (resConseling: any) => {
        let counter = 0
        let current_user = null
        let consel_id = null
        let tempArr = []
        let tempRes = []
        if(resConseling && resConseling.length > 0) {
          await resConseling.map((consel, index) => {
            if(consel.patient.id != current_user) {
              if(counter > 0) {
                consel.counter = counter
              }
              current_user = consel.patient.id
              counter = 1
              consel_id = consel.id
            } else {
              counter ++
            }
          });
          if(counter > 0) {
            resConseling[resConseling.findIndex(cons => cons.id == consel_id)].counter = counter
          }
        }

        this.listConseling = resConseling
        console.log(resConseling)
      })
    } catch (error) {
      console.log(error)
    } finally {
      setTimeout(() => {
        this.onload = false
      }, 2000)
    }
  }

  getCategory(id) {
    let category;
    this.categories.forEach(element => {
      if(element._id == id) {
        category = element.category
      }
    });
    return category;  
  }

  async openResult(data) {
    console.log(data)
    const modal = await this.modalCtrl.create({
      component: ConselingResultPage,
      componentProps: {
        dataConseling: data
      }
    });
    modal.onDidDismiss().then((res: any) => {
      if(res.data) {
        data = res.data
        window.location.reload()
      }
    })
    return await modal.present();
  }

  async openLogChat(data) {
    console.log(data)
    let uri = "room-chat/"+data.id
    console.log(uri)
    let navigationExtras: NavigationExtras = {
      queryParams: {
        conseling: uri
      }
    };
    this.router.navigateByUrl(uri)
  }
}
