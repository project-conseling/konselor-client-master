import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProvidersService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-conseling-result',
  templateUrl: './conseling-result.page.html',
  styleUrls: ['./conseling-result.page.scss'],
})
export class ConselingResultPage implements OnInit {
  @Input() dataConseling: any;
  @Input() category: any;
  onload: boolean = false
  formResult = null
  constructor(private modalCtrl: ModalController, private api: ProvidersService) { }

  ngOnInit() {
    this.onload = true;
    setTimeout(() => {
      this.onload = false;
    })
    console.log(this.dataConseling)
    
  }

  closeModal() {
    this.modalCtrl.dismiss()
  }
  async putConseling(){
    this.dataConseling.result = await this.formResult
    this.dataConseling.status = await 8
    this.api.setResult(this.dataConseling)
    .subscribe((resp: any) => {
      console.log(resp)
      this.modalCtrl.dismiss(resp)
    })
  }

}
