import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import Swal from 'sweetalert2';
import { ProvidersService } from 'src/app/services/providers.service';
import { CalendarComponentOptions } from 'ion2-calendar';

@Component({
  selector: 'app-set-schedule',
  templateUrl: './set-schedule.page.html',
  styleUrls: ['./set-schedule.page.scss'],
})
export class SetSchedulePage implements OnInit {
  @Input() dates: any = [];
  @Input() isPost: true;
  @Input() dateWillUpdate: any;
  @Input() timer: any = []

  customPickerOptions: any;
  

  formDate = {
    conselor_id: "",
    date: "",
    time: []
  }
  selectedValues = []
  disabledTime = false
  clock = [
    { time: "09:00", status: 0 },
    { time: "10:00", status: 0 },
    { time: "11:00", status: 0 },
    { time: "12:00", status: 0 },
    { time: "13:00", status: 0 },
    { time: "14:00", status: 0 },
    { time: "15:00", status: 0 },
    { time: "16:00", status: 0 },
    { time: "17:00", status: 0 },
    { time: "18:00", status: 0 },
    { time: "19:00", status: 0 },
    { time: "20:00", status: 0 },
    { time: "21:00", status: 0 },
    { time: "22:00", status: 0 }
  ]

  sessionOne = null
  
  isSession_two = false
  sessionTwo = null
  sessionTwoFrom = null
  sessionTwoHour = [9,10,11,12,13,14,15,16,17,18]

  isSession_three = false
  sessionThree = null
  sessionThreeFrom = null
  sessionThreeHour = [9,10,11,12,13,14,15,16,17,18]



  selectedDate = "";
  times = [];
  timeChanged = false;
  constructor(private modalCtrl: ModalController, private api: ProvidersService) { 
    this.customPickerOptions = {
      buttons: [{
        text: 'Save',
        handler: () => console.log(this.sessionOne)
      }]
    }
  }

  ngOnInit() {
    this.formDate.conselor_id = JSON.parse(localStorage.getItem("_USER"))._ID
    console.log("date",this.dateWillUpdate)
    console.log("dates => ", this.dates)
    if(this.timer.length > 0) {
      
      this.timer.map((time, index) => {
        if(index == 0) {
          this.sessionOne = this.dateWillUpdate +"T"+ time.time
          var dump = this.getTimeSessOne(this.sessionOne)
        }
        if(index == 1) {
          this.sessionTwo = this.dateWillUpdate +"T"+ time.time
          var dump =  this.getTimeSessTwo(this.sessionTwo)
          this.isSession_two = true
        }
        if(index == 2) {
          this.sessionThree = this.dateWillUpdate +"T"+ time.time
          var dump =  this.getTimeSessTwo(this.sessionThree)
          this.isSession_three = true
        }
        this.selectedValues.push(time.time)
      })
    }
    console.log("wiil",this.dateWillUpdate)
    let nowDate = new Date()
    this.selectedDate = this.dateWillUpdate
    this.formDate.time = this.clock;
    // if(this.timer.length == 0) {
    //   this.selectDate()
    // }
  }
  closeModal() {
    this.modalCtrl.dismiss()
  }

  doSubmit() {
    console.log(this.clock)
  }

  doSave() {
    
    let formSchedule = []
    if(this.sessionOne) {
      formSchedule.push({
        conselor_id: this.formDate.conselor_id,
        time: this.getTime(this.sessionOne),
        schedule_date: this.selectedDate,
        status: 0,
      })
    } 
    if(this.isSession_two && this.sessionTwo) {
      formSchedule.push({
        conselor_id: this.formDate.conselor_id,
        time: this.getTime(this.sessionTwo),
        schedule_date: this.selectedDate,
        status: 0,
      })
    }

    if(this.isSession_three && this.sessionThree) {
      formSchedule.push({
        conselor_id: this.formDate.conselor_id,
        time: this.getTime(this.sessionThree),
        schedule_date: this.selectedDate,
        status: 0,
      })
    }
    let timerInterval
    Swal.fire({
      title: '<strong>Mohon Tunggu</strong>',
      text: 'Sedang dalam proses',
      timer: 3000,
      onBeforeOpen: () => {
        Swal.showLoading();
        timerInterval = setInterval(() => {
        }, 3000)
      },
      onOpen: async () => {
        Swal.stopTimer();
        try {
          this.api.setSchedule(formSchedule)
            .subscribe((resp: any) => {
              console.log(resp);
              this.dates.push(resp.data)
            })
        } catch (error) {
          console.log("error => ", error)
        } finally {
          Swal.resumeTimer();
        }
        
      },
      onClose: () => {
        this.modalCtrl.dismiss()
        clearInterval(timerInterval)
      }
    })
    
    
  }

  selectDate() {
    let id = JSON.parse(localStorage.getItem("_USER"))._ID
    this.api.getMyScheduleByDate(id, this.selectedDate)
    .subscribe((res: any) => {
      if(res.length > 0) {
        res.map((time, index) => {
          if(index == 0) {
            this.sessionOne = this.dateWillUpdate +"T"+ time.time
            var dump = this.getTimeSessOne(this.sessionOne)
          }
          if(index == 1) {
            this.sessionTwo = this.dateWillUpdate +"T"+ time.time
            var dump =  this.getTimeSessTwo(this.sessionTwo)
            this.isSession_two = true
          }
          if(index == 2) {
            this.sessionThree = this.dateWillUpdate +"T"+ time.time
            var dump =  this.getTimeSessTwo(this.sessionThree)
            this.isSession_three = true
          }
        }) 
      } else {
        this.removeSession(1)
      }
    })
  }

  pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }

  checkTime(time) {

  }

  timeSelected() {
    console.log("selected date")
    // let counter = 0
    // this.formDate.time.map((tm, index) => {
    //   if(tm.status) {
    //     counter ++
    //   }
    // })
    // if(counter == 3) {
    //   this.disabledTime = true
    // }
  }

  getScheduler() {

  }

  sessionOneSetter() {
    console.log(this.sessionOne)
  }

  getTime(dateTime) {
    let time = dateTime.split('T')[1]
    let min = time.split(":")[1]
    let hour = time.split(":")[0]
    return this.pad(parseInt(hour), 2) +":"+ this.pad(parseInt(min), 2)
  }

  getTimeSessOne(dateTime) {
    let time = dateTime.split('T')[1]
    let min = time.split(":")[1]
    let hour = time.split(":")[0]
    
    if((parseInt(min) + 90) > 60 ) {
      hour = parseInt(hour) + 1
      min = (parseInt(min) + 90) - 60
      if(min >= 60) {
        hour = hour + 1
        min = min - 60
      }
    }
    this.sessionTwoFrom = hour
    let hourSelection = []
    this.sessionTwoHour.map(h => {
      if(h > hour) {
        hourSelection.push(h)
      }
    })
    this.sessionTwoHour = hourSelection

   return this.pad(parseInt(hour), 2) +":"+ this.pad(parseInt(min), 2)
  }

  getTimeSessTwo(dateTime) {
    let time = dateTime.split('T')[1]
    let min = time.split(":")[1]
    let hour = time.split(":")[0]
    
    if((parseInt(min) + 90) > 60 ) {
      hour = parseInt(hour) + 1
      min = (parseInt(min) + 90) - 60
      if(min > 60) {
        hour = hour + 1
        min = min - 60
      }
    }
    let hourSelection = []
    this.sessionThreeHour.map(h => {
      if(h > hour) {
        hourSelection.push(h)
      }
    })
    this.sessionThreeHour = hourSelection
    this.sessionThreeFrom = hour

   return this.pad(parseInt(hour), 2) +":"+ this.pad(parseInt(min), 2)
  }

  getTimeSessThree(dateTime) {
    let time = dateTime.split('T')[1]
    let min = time.split(":")[1]
    let hour = time.split(":")[0]
    
    if((parseInt(min) + 90) > 60 ) {
      hour = parseInt(hour) + 1
      min = (parseInt(min) + 90) - 60
      if(min > 60) {
        hour = hour + 1
        min = min - 60
      }
    }
   return this.pad(parseInt(hour), 2) +":"+ this.pad(parseInt(min), 2)
  }

  addSession(sess) {
    switch (sess) {
      case 2:
        this.isSession_two = (this.sessionTwoFrom > 18 || this.sessionTwoFrom == 18 ) ? false : true
        break;
      case 3:
        this.isSession_three = (this.sessionThreeFrom > 18 || this.sessionThreeFrom == 18) ? false : true
        break;
      default:
        break;
    }
  }

  removeSession(sess) {
    switch (sess) {
      case 2:
        this.isSession_two = false
        this.sessionTwoHour = [9,10,11,12,13,14,15,16,17,18]
        this.sessionTwo = null
        this.sessionThreeFrom = null
        break;
      case 3:
        this.isSession_three = false
        this.sessionThreeHour = [9,10,11,12,13,14,15,16,17,18]
        this.sessionThree = null
        break;
      case 1:
        this.isSession_two = false
        this.isSession_three = false
        this.sessionTwoHour = [9,10,11,12,13,14,15,16,17,18]
        this.sessionThreeHour = [9,10,11,12,13,14,15,16,17,18]
        this.sessionTwoFrom = null
        this.sessionThreeFrom = null
        this.sessionOne = null
        this.sessionTwo = null
        this.sessionThree = null
        break;
      default:
        break;
    }
  }
  

}
