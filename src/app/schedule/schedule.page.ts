import { Component, OnInit, Input } from '@angular/core';
import { CalendarComponentOptions, DayConfig } from 'ion2-calendar'
import { ModalController } from '@ionic/angular';
import { UserService } from '../user/user.service';
import { ProvidersService } from '../services/providers.service';
import { SetSchedulePage } from './set-schedule/set-schedule.page';
import { NavigationExtras, Router } from '@angular/router';
import { RTCService } from '../rtc.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.page.html',
  styleUrls: ['./schedule.page.scss'],
})
export class SchedulePage implements OnInit {
  @Input() weekly: any;
  @Input() weekId: string;
  @Input() configDays: DayConfig[];
  isChanged = false;

  onload: boolean = false;
  
  optionsCalendar: CalendarComponentOptions = {
    showMonthPicker: false,
    from: new Date(1),
    weekdays: ['minggu','senin','selasa','rabu','kamis','jumat','sabtu'],
    monthFormat: 'MMMM YYYY',
    daysConfig: []

  };

  patient = [];
  dates = [];
  times = [];
  selectedDate = "";
  firstTime = true;
  text_null = "Silahkan pilih Edit Jadwal Untuk menambahkan jadwal Anda"
  multiDate = []
  dateWillEdit = ""

  mySchedule = []
  dataConseling = []
  constructor(private apiUser: UserService, 
    private modalCtrl: ModalController, 
    private router: Router,
    public rtcService: RTCService,
    private api: ProvidersService) { }

  ngOnInit() {
    
    this.onload = true
    let _daysConfig: DayConfig[] = [];
    // this.optionsCalendar.daysConfig = _daysConfig
    console.log(this.weekly)
    if(localStorage.getItem('_firstTime') == "true") {
      this.firstTime = true;
      localStorage.removeItem('_firstTime')
      localStorage.setItem('_firstTime', "false")
    } else {
      this.firstTime = false;
    }
    this.times = [];
    let nowDate = new Date();
    this.selectedDate = nowDate.getFullYear()+"-"+this.pad(nowDate.getMonth()+1, 2)+"-"+this.pad(nowDate.getDate(), 2)
    console.log(this.selectedDate)
    let id = JSON.parse(localStorage.getItem("_USER"))._ID
    let tempDates = []
    try {
      this.api.getMySchedule(id)
      .subscribe((res: any) => {
        console.log(res)
        this.mySchedule = res
        if(res.length > 0) {
          this.showPatient()
          res.forEach(element => {
            if(element.schedule_date == this.selectedDate) {
              this.times.push({
                status: element.status,
                time: element.time
              })
            }
            this.multiDate.push(element.date)
            if(element.status == 1) {
               _daysConfig.push(
                {
                  date: new Date(element.schedule_date),
                  subTitle: "•",
                  marked: true
                }
              )
            } else if ( element.status == 0 ) {
              _daysConfig.push(
                {
                  date: new Date(element.schedule_date),
                  marked: true
                }
              )
            }
          });
          console.log("day configur", _daysConfig )
          this.optionsCalendar.daysConfig = _daysConfig;
        }
      })
      this.dateWillEdit = this.selectedDate
      
    } catch (error) {
      console.log("error sc page", error)
    } finally {
      setTimeout(() => {
        this.onload = false;
      }, 4000)
    }
  }

  

  doSubmit() {
    let id = JSON.parse(localStorage.getItem("_USER"))._ID
    let form = {
      week: this.weekly,
      _id: id
    }
    this.apiUser.putWeekly(form).subscribe(res => console.log(res))
    this.isChanged = false
  }

  doChange() {
    this.isChanged = true;
  }

  selectDate() {
    this.showPatient()
    this.times = [];
    this.mySchedule.forEach(element => {
      if(element.schedule_date == this.selectedDate) {
        // element.time.forEach(time => {
        //   time.status != 0 ? this.times.push(time) : '';
        // });
        this.times.push({
          status: element.status,
          time: element.time
        })
      }
    });
  }

  fetchData() {
    this.times = [];
    let nowDate = new Date();
    this.selectedDate = nowDate.getFullYear()+"-"+this.pad(nowDate.getMonth()+1, 2)+"-"+this.pad(nowDate.getDate(), 2)
    let id = JSON.parse(localStorage.getItem("_USER"))._ID
    this.api.getMySchedule(id)
    .subscribe((res: any) => {
      this.dates = res
      this.dates.forEach(element => {
        if(element.schedule_date == this.selectedDate) {
          // element.time.forEach(time => {
          //   time.status != 0 ? this.times.push(time) : '';
          // });
          element.status != 0 ?
          this.times.push(element.time) : ''
        }
      });
    })
  }
  async setSchedule() {
    const modal = await this.modalCtrl.create({
      component: SetSchedulePage,
      componentProps: {
        dates: this.dates,
        dateWillUpdate: this.selectedDate,
        timer: this.times
      }
    });
    modal.onDidDismiss().then(() => {
      this.ngOnInit()
    })
    return await modal.present();
  }
  pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }

  showPatient() {
    this.patient = []
    console.log(this.mySchedule)
    this.mySchedule.forEach((data: any) => {
      if(data.schedule_date == this.selectedDate && data.status == 1 ) {
        this.patient.push(data.conseling)
      }
    })
  }

  openChatRoom(data) {
    let methode = data.methode

    switch (methode) {
      case "Chat":
        let uri = "room-chat/"+data.id
        console.log(uri)
        let navigationExtras: NavigationExtras = {
          queryParams: {
            conseling: uri
          }
        };
        this.router.navigateByUrl(uri)
        break;
      case "Call":
          this.rtcService.userInteraction.callee = data.patient.profile
          this.rtcService.userInteraction.caller = data.conselor.profile
          this.rtcService.pushCallAudio(data.patient.id)
          // this.rtcService.pushCall(data.patient.id)
          this.router.navigateByUrl("caller")
        break;
      case "Video Call":
          this.rtcService.userInteraction.callee = data.patient.profile
          this.rtcService.userInteraction.caller = data.conselor.profile
          this.rtcService.pushCall(data.patient.id)
          this.router.navigateByUrl('vidcall')
        break;
      default:
        break;
    }
    
  }

  startVidCall(id) {
    this.rtcService.pushCall(id)
    this.router.navigateByUrl('vidcall')
  }
  startCall(id) {
    this.rtcService.pushCallAudio(id)
    this.router.navigateByUrl('caller')
    // this.modalCtrl.dismiss({state: "makeCall", to: id})
    // console.log(id)
  }
}
