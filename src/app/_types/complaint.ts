import { MyProfile } from './my.profile';

export class Complaint {
    category_id: any;
    conselorId: any;
    created_on: string;
    note: string;
    patientId: any;
    profile_conselor: MyProfile;
    status: number;
    story: string;
    subyek: string;
    __v: number;
    _id: any;
}