import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CalendarModule } from 'ion2-calendar';
import { SocketIoModule, SocketIoConfig } from 'ng-socket-io';
import { ReactiveFormsModule } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Deeplinks } from '@ionic-native/deeplinks/ngx';

// const config: SocketIoConfig = {url: 'http://api.konselor.my.id', options: {}};
const config: SocketIoConfig = {url: 'https://conselings.herokuapp.com', options: {}};
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    HttpClientModule,
    BrowserModule, 
    ReactiveFormsModule,
    CalendarModule,
    IonicModule.forRoot(), 
    SocketIoModule.forRoot(config),
    AppRoutingModule],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    FilePath,
    Deeplinks,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
