import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProvidersService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-form-answer',
  templateUrl: './form-answer.page.html',
  styleUrls: ['./form-answer.page.scss'],
})
export class FormAnswerPage implements OnInit {

  constructor(private route: ActivatedRoute, private api: ProvidersService, private router: Router) { }
  question: any
  ngOnInit() {
    let idQuestion = this.route.snapshot.params.id;
    this.api.getOneQuestion(idQuestion)
    .subscribe((question: any) => {
      this.question = question
      console.log(question)
    })
  }

  ionViewWillEnter() {
    this.ngOnInit()
  }

  doBack() {
    this.router.navigateByUrl('cs')
  }

  answerQuestion() {
    this.api.modifyQuestion(this.question)
    .subscribe((resp: any) => {
      this.router.navigateByUrl('cs')
    })
  }

}
