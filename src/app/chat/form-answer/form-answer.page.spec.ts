import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormAnswerPage } from './form-answer.page';

describe('FormAnswerPage', () => {
  let component: FormAnswerPage;
  let fixture: ComponentFixture<FormAnswerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormAnswerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormAnswerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
