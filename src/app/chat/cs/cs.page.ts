import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProvidersService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-cs',
  templateUrl: './cs.page.html',
  styleUrls: ['./cs.page.scss'],
})
export class CsPage implements OnInit {

  constructor(public router: Router, private api: ProvidersService) { }
  listQuestion = []

  ngOnInit() {
    this.listQuestion = []
    this.functionStarter()
  }

  functionStarter() {
    this.api.getListQuestion()
    .subscribe((resp: any) => {
      console.log(resp)
      if(resp && resp.length > 0) {
        resp.forEach(quest => {
          if(quest.answered == null) {
            this.listQuestion.push(quest)
          }
        })
      }
    })
  }

  navigateTo() {
    this.router.navigateByUrl('login')
  }

  toAnswer(quest) {
    // let uri = "form-answer/"+data.id+"-"+data.patient.id
    // console.log(uri)
    // let navigationExtras: NavigationExtras = {
    //   queryParams: {
    //     conseling: uri
    //   }
    // };
    // this.router.navigateByUrl(uri)
    this.router.navigateByUrl('form-answer/'+quest.id)
  }

}
