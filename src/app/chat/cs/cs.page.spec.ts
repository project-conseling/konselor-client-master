import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsPage } from './cs.page';

describe('CsPage', () => {
  let component: CsPage;
  let fixture: ComponentFixture<CsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
