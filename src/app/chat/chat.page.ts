import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ProvidersService } from '../services/providers.service';
import { Category } from '../_types/category';
import { BackendSocketService } from '../backend-socket.service';
import { Router, NavigationExtras } from '@angular/router';
import { RTCService } from '../rtc.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  onload: boolean = false;
  listConseling: any = [];
  categories: Category[];
  constructor(public api: ProvidersService, 
    public modalCtrl: ModalController, 
    private router: Router,
    public rtcService: RTCService,
    public socketService: BackendSocketService) { }

  async ngOnInit() {
    this.onload = true
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID;
    try {
      this.api.getMyConseling(id)
      .subscribe((resp: any) => {
        console.log(resp)
        this.listConseling = []
        if(resp && resp.length > 0) {
          resp.forEach(conseling => {
            if(conseling.result == null) {
              this.listConseling.push(conseling)
            }
          })
        }
      })
      this.socketService.getActiveUser().subscribe(async (dataObs: any) => {
        if(dataObs.event == 'joined') {
          const i = await this.socketService.activeId.findIndex(item => item === dataObs.user);
          if(i > -1){
  
          } else {
            this.socketService.activeId.push(dataObs.user)
          }
        } else {
          const i = this.socketService.activeId.findIndex(item => item === dataObs.user);
          if(i > -1){
            this.socketService.activeId.splice(i, 1)
          } else {
            // this.activeId.push(dataObs.user)
          }
        }
      })
    } catch (error) {
      console.log(error)
    } finally {
      setTimeout(() => {
        this.onload = false
      }, 2000)
    }
    // await this.api.getCategories()
    // .subscribe( async (resp: any) => {
    //   this.categories = await resp.data
    //   await this.api.getMyConseling(id)
    //   .subscribe( async (resConseling: any) => {
    //     if(resConseling.data.length > 0) {
    //       await resConseling.data.forEach(async conseling => {
    //         await this.api.getComplaintId(conseling.complaint_id)
    //         .subscribe(async (respComplaint: any) => {
    //           conseling.complaint_data = await respComplaint.data
    //           await this.api.getMyScheduleConseling(conseling._id)
    //           .subscribe(async (respSchedule: any) => {
    //             conseling.schedule = await respSchedule.data[0];
    //             await this.api.getProfile(conseling.patientId)
    //             .subscribe(async (respUser: any) => {
    //               conseling.patient = await respUser.data[0]
    //             })
    //           })
    //           if(conseling.status != 9) {
    //             this.listConseling.push(conseling)
    //           }
    //         })
    //         // console.log(conseling)
    //       });
    //     }
    //     this.socketService.getActiveUser().subscribe(async (dataObs: any) => {
    //       if(dataObs.event == 'joined') {
    //         const i = await this.socketService.activeId.findIndex(item => item === dataObs.user);
    //         if(i > -1){
    
    //         } else {
    //           this.socketService.activeId.push(dataObs.user)
    //         }
    //       } else {
    //         const i = this.socketService.activeId.findIndex(item => item === dataObs.user);
    //         if(i > -1){
    //           this.socketService.activeId.splice(i, 1)
    //         } else {
    //           // this.activeId.push(dataObs.user)
    //         }
    //       }
    //     })
    //     setTimeout(() => {
    //       this.onload = false
    //     }, 2000)
    //     console.log(this.listConseling)
    //   })
    // })
  }

  checkUser(id) {
    const i = this.socketService.activeId.findIndex(item => item === id);
    if(i > -1){
      return true;
    } else {
      return false;
    }
  }

  openChatRoom(data) {
    let methode = data.methode

    switch (methode) {
      case "Chat":
        let uri = "room-chat/"+data.id
        console.log(uri)
        let navigationExtras: NavigationExtras = {
          queryParams: {
            conseling: uri
          }
        };
        this.router.navigateByUrl(uri)
        break;
      case "Call":
          this.rtcService.userInteraction.callee = data.patient.profile
          this.rtcService.userInteraction.caller = data.conselor.profile
          // this.rtcService.pushCallAudio(data.patient.id)
          this.rtcService.pushCall(data.patient.id)
          this.router.navigateByUrl("caller")
        break;
      case "Video Call":
          this.rtcService.userInteraction.callee = data.patient.profile
          this.rtcService.userInteraction.caller = data.conselor.profile
          this.rtcService.pushCall(data.patient.id)
          this.router.navigateByUrl('vidcall')
        break;
      default:
        break;
    }
    
  }

}
