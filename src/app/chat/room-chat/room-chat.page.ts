import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import Swal from 'sweetalert2';
import { Socket } from 'ng-socket-io';
import { Observable } from 'rxjs';
import { BackendSocketService } from 'src/app/backend-socket.service';
import { ProvidersService } from 'src/app/services/providers.service';
import { RTCService } from 'src/app/rtc.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Complaint } from 'src/app/_types/complaint';

@Component({
  selector: 'app-room-chat',
  templateUrl: './room-chat.page.html',
  styleUrls: ['./room-chat.page.scss'],
})
export class RoomChatPage implements OnInit {
  @ViewChild('IonContent') content: IonContent
  dataComplaint: Complaint
  complaint_id: any;
  dataConseling: any;
  msgList: any = [];
  onload: boolean = false;
  paramData: any;
  userName: any;
  myName: any;
  profile_patient: any
  user_input: string = "";
  User: string ;
  toUser: string ;
  start_typing: any;
  loader: boolean;
  profile: any;
  activedToUser: boolean = false;

  conselings: any
  constructor(private api: ProvidersService, public socketApi: BackendSocketService, private route: ActivatedRoute,
    private socket: Socket, public rtcService: RTCService, private router: Router) {}

  async ngOnInit() {
    this.onload = true;
    let param = this.route.snapshot.params.id;
    this.complaint_id = param
    // this.route.queryParams.subscribe(params => {
    //   console.log(params)
    // });
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.User = id
    this.socketApi.statusActive()
    this.msgList = []
    await this.api.getOneConseling(this.complaint_id)
    .subscribe((resp: any) => {
      console.log(resp)
      this.profile_patient = resp.patient
      this.toUser = resp.patient.id
      this.conselings = resp
      if(resp && resp.chat_rooms && resp.chat_rooms.length > 0) {
        resp.chat_rooms.forEach(chat => {
          let chipTime = chat.createdAt.split('T')[1].split(":")[0]+ ":" + chat.createdAt.split('T')[1].split(":")[1]
          console.log(chipTime)
          let chatData = {
            userId: chat.user_id,
            userName: chat.user.profile.name,
            userAvatar: chat.user.profile.avatar,
            time: chipTime,
            message: chat.chat,
            isReaded: chat.isReaded,
            id: chat.id
          }
          this.msgList.push(chatData)
        });
      }
    })
    await setTimeout(() => {
      this.onload = false
    }, 1000)
    this.scrollDown()
    // this.api.getProfile(this.toUser)
    // .subscribe(async (resp: any) => {
    //   this.profile_patient = resp.data[0]
    //   this.api.getProfile(id)
    //     .subscribe(async (prop: any) => {
    //       this.profile = await prop.data[0]
    //       await this.fetchData()
    //       setTimeout(() => {
    //         this.content.scrollToBottom(300)
    //         this.onload = false
    //       }, 1500)
    //   })
    // })
    
    this.statusActive()
    this.socketApi.getMessages()
    .subscribe(async (dataObs: any) => {
      console.log(dataObs)
      if(dataObs.data.conseling_id == this.complaint_id) {
        await this.fetchData(dataObs)
        this.scrollDown()
      }
    })

    this.socketApi.getActiveUser().subscribe((dataObs: any) => {
      if(dataObs.user == this.toUser && dataObs.event == "joined") {
        this.activedToUser = true
      } else if(dataObs.user == this.toUser && dataObs.event == "left"){
        this.activedToUser = false
      }
    })
  }
  ionViewDidEnter() {
    if(this.msgList.length > 0) {
      console.log("Chatter",this.msgList)
      this.msgList.map(chat => {
        if(chat.userId != this.User) {
          this.api.readChat(chat.id)
          .subscribe(resp => resp)
        }
      })
      // console.log()
    }

  }

  startVidCall(id) {
    this.rtcService.pushCall(id)
    this.router.navigateByUrl('vidcall')
  }
  statusActive() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.socket.emit('set-nickname', id)
    setTimeout(() => {
      this.statusActive()
    }, 3000)
  }
  startCall(id) {
    this.rtcService.pushCallAudio(id)
    this.router.navigateByUrl('caller')
    // this.modalCtrl.dismiss({state: "makeCall", to: id})
    // console.log(id)
  }

  sendMsg() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    let time = new Date()
    let tempTime = this.pad(time.getHours(),2)+":"+this.pad(time.getMinutes(),2)
    // let formMsg = {
    //   complaint_id: this.complaint_id,
    //   user_id: id,
    //   avatar: this.profile.avatar,
    //   name: this.profile.name,
    //   text: this.user_input,
    //   time: tempTime
    // }
    console.log(this.user_input)
    if (this.user_input !== '') {
      
      this.api.sendChat({
        conseling_id: this.complaint_id,
        chat: this.user_input,
        user_id: this.User,
        isReaded: 0
      })
      .subscribe((res: any) => {
        this.socket.emit('add-message', 
          {
            data: res
          })
          this.user_input = ''
      })
      // this.api.sendChat(formMsg)
      // .subscribe((resp: any) => {
      //   console.log(resp)
        
      // })
      // this.user_input = "";
      // this.scrollDown()
      // setTimeout(() => {
      //   this.content.scrollToBottom(300)
      //   // this.senderSends()
      // }, 500);
    }
  }

  // senderSends(data: any) {
  //   this.loader = true;
  //   console.log(data)
  //   setTimeout(() => {
  //     this.msgList.push({
  //       userId: this.User,
  //       userName: this.User,
  //       userAvatar: data.userAvatar,
  //       time: data.time,
  //       message: data.message
  //     });
  //     this.loader = false;
  //     this.scrollDown()
  //   }, 2000)
  //   this.scrollDown()
  // }
  getMessages() {
    let observable = new Observable(obs => {
      this.socket.on('message', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }
  scrollDown() {
    setTimeout(() => {
      this.content.scrollToBottom(50)
    }, 50);
  }

  pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }

  fetchData(dataObs) {
    console.log(dataObs.data)
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.User = id
    this.api.getOneConseling(this.complaint_id)
    .subscribe((resp: any) => {
      console.log(resp)
      this.profile_patient = resp.patient
      this.toUser = resp.patient.id
      if(resp && resp.chat_rooms && resp.chat_rooms.length > 0) {
        resp.chat_rooms.forEach(chat => {
          let chipTime = chat.createdAt.split('T')[1].split(":")[0]+ ":" + chat.createdAt.split('T')[1].split(":")[1]
          console.log(chipTime)
          let chatData = {
            userId: chat.user_id,
            userName: chat.user.profile.name,
            userAvatar: chat.user.profile.avatar,
            time: chipTime,
            message: chat.chat,
            isReaded: chat.isReaded,
            id: chat.id
          }
          if(dataObs.data.id == chat.id) {
            this.msgList.push(chatData)
          }
        });
      }
    })
    
  }

  navigateTo(page) {
    this.router.navigateByUrl('home')
  }

  checkUser(id) {
    const i = this.socketApi.activeId.findIndex(item => item === id);
    if(i > -1){
      return true;
    } else {
      return false;
    }
  }

}
