import { Component, OnInit } from '@angular/core';
import { RTCService } from 'src/app/rtc.service';
import { ProvidersService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-caller',
  templateUrl: './caller.page.html',
  styleUrls: ['./caller.page.scss'],
})
export class CallerPage implements OnInit {

  user;

  onload: boolean = false;
  constructor(public rtcService: RTCService, private api: ProvidersService) { }

  ngOnInit() {
    console.log(this.rtcService.getSession())
    this.user = this.rtcService.getUser()
    console.log(this.user)
  }


  answere() {
    this.rtcService.acceptCall()
  }

  decline() {
    this.rtcService.decline()
    window.history.back();
  }

  refuse() {
    this.rtcService.refuse()
  }

  getState() {
    if(this.rtcService.onstate == 'incoming') {
      return "Panggilan Masuk"
    } else if(this.rtcService.onstate == 'call') {
      return "Menghubungkan"
    } else if(this.rtcService.onstate == 'accept') {
      return "Terhubung"
    }
  }

  setVolume() {

  }

  setMic() {
    // this.rtcService.toogleMute()
  }
  

}
