import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {

  constructor(private route: ActivatedRoute, private api: UserService) { }
  pwd = null
  pwdConfirm = null
  logo: any;
  token = null
  async ngOnInit() {
    this.logo = '../../../assets/images/logo-cons.png'
    let next = await this.route.snapshot.queryParams
    this.token = next
    
    console.log(next)
  }
  doChange() {
    let formReset = {
      password: this.pwdConfirm
    }
    let timerInterval;
    Swal.fire({
      title: '<strong>Mohon Tunggu</strong>',
      text: 'Sedang dalam proses',
      timer: 3000,
      onBeforeOpen: () => {
        Swal.showLoading();
        timerInterval = setInterval(() => {
        }, 3000)
      },
      onOpen: async () => {
        Swal.stopTimer();
        try {
          this.api.resetMyPassword(formReset, this.token)
          .subscribe((resp: any) => {
            if(resp && resp.success) {
            } else {
              Swal.fire('Oops...', 'Maaf terjadi kesalahan!', 'error')
            }
          })
        } catch (error) {
          console.log(error)
          Swal.fire('Oops...', 'Maaf terjadi kesalahan!', 'error')
        }
        
        Swal.resumeTimer();
      },
      onClose: () => {
        Swal.fire('Berhasil', 'Silahkan login dengan kata sandi baru!', 'success').then(() => {
          window.location.href = '/login'
        })
        clearInterval(timerInterval)
      }
    })
    
    console.log("click")
  }

}
