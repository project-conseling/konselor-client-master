import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment'
import { HttpClient, HttpHeaders } from '@angular/common/http';
const ENV = environment.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private  httpClient:  HttpClient) { }
  register(newUser: any) {
    console.log(newUser)
    let form = newUser;
    return this.httpClient.post(`${ENV}/user/register`, form);
  }

  resetMyPassword(form, token) {
    return this.httpClient.post(`${ENV}/user/reset-password`, form, {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${token}`
      })
      
    });
  }

  login(user: any) {
    return this.httpClient.post(`${ENV}/user/login`, user)
  }

  getUser(id) {
    return this.httpClient.get(`${ENV}/user/`+id)
  }

  async logout() {
    localStorage.clear()
  }

  sendEmail(form) {
    return this.httpClient.post(`${ENV}/forget-password/send-email`, form)
  }

  updateUser(form) {
    return this.httpClient.put(`${ENV}/user/update/`+form._id, form)
  }

  updateProfile(profile: any) {
    return this.httpClient.put(`${ENV}/profile/${profile._id}`, profile)
  }

  addSpecialist(req) {
    return this.httpClient.post(`${ENV}/specialist`, req)
  }

  getConselorBySpecialist(id) {
    return this.httpClient.get(`${ENV}/specialist/conselor/${id}`)
  }

  postWeekly(req) {
    return this.httpClient.post(`${ENV}/schedule/weekly`, req)
  }
  putWeekly(req) {
    let week = {
      week: req.week
    }
    return this.httpClient.put(`${ENV}/schedule/weekly/${req._id}`, week)
  }
  getWeekly(id) {
    return this.httpClient.get(`${ENV}/schedule/weekly/${id}`)
  }
  getProfile(id) {
    return this.httpClient.get(`${ENV}/profile/${id}`);
  }

  forgetPassword(form) {
    return this.httpClient.post(`${ENV}/forget-password/email-checking`, form);
  }
}
