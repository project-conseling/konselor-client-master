import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.page.html',
  styleUrls: ['./forget-password.page.scss'],
})
export class ForgetPasswordPage implements OnInit {
  formEmail = {
    email: ''
  }

  constructor(private router: Router, private api: UserService) { }

  ngOnInit() {
  }

  navigateTo(page) {
    switch (page) {
      case 'login':
        this.router.navigateByUrl('login')
        break;
      
      default:
        break;
    }
  }

  doSend() {
    
    // this.router.navigateByUrl('login')
    this.api.forgetPassword(this.formEmail)
    .subscribe((resp: any) => {
      if(resp.success) {
        Swal.fire(
          'Terima Kasih!',
          'Untuk melanjutkan setting ulang kata sandi anda, silahkan cek email anda!',
          'success'
        )
      }
    })
  }


}
