import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery'
import Swal from 'sweetalert2';
// import { ServicesService } from '../services.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { CategoryServiceService } from 'src/app/services/category-service.service';
import { ConselorFormPage } from '../conselor-form/conselor-form.page';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { File as Files, FileEntry } from '@ionic-native/File/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { CloudinaryServiceService } from 'src/app/services/cloudinary-service.service';
import { UserService } from '../user.service';
import { CloudinaryService } from 'src/app/services/cloudinary.service';
import { ProvidersService } from 'src/app/services/providers.service';

@Component({
  selector: 'app-registrasi',
  templateUrl: './registrasi.page.html',
  styleUrls: ['./registrasi.page.scss'],
})
export class RegistrasiPage implements OnInit {
  formRegist = {
    email: '',
    name: '',
    birth: '',
    hp: '',
    ktp: '',
    address: '',
    gender: 'men',
    avatar: '',
    password: '',
    confirmPassword: '',
    role: 1,
    usia: 0,
    agama: "",
    marital_status: "",
    birth_place: "",
    job: ""
  }

  bod: '';
  file: File;
  fileKtp: File;
  ktpName = ''
  fotoName = ''

  categories = [];

  urlPhoto = ''
  urlKtp = ''
  idPhoto: any;
  idKtp: any;
  constructor(private router: Router, public formBuilder: FormBuilder, private apiUser : UserService,
    private route: ActivatedRoute, private modalCtrl: ModalController, private api: ProvidersService, 
    private camera: Camera,  private filePath: FilePath, private actionSheetCtrl: ActionSheetController,
    private clodinary: CloudinaryService) { }

  ngOnInit() {

    this.api.getCategories()
    .subscribe((res: any) => {
      let category;
      res.forEach(item => {
        category = {
          id: item.id,
          text: item.category,
          checked: false
        }
        this.categories.push(category)
      });
    })
  }
  
  changeListener($event) : void {
      if($event.target.files[0]) {
        this.file = $event.target.files[0];
        if($event.target.files && $event.target.files[0]){
          let reader = new FileReader();
          reader.readAsDataURL($event.target.files[0]);
        }
          let fileList: FileList = $event.target.files;  
          // this.fotoName = fileList[0].name
      }
    }
  changeListenerKtp($event) : void {
    if($event.target.files[0]) {
      this.fileKtp = $event.target.files[0];
      if($event.target.files && $event.target.files[0]){
        let reader = new FileReader();
        reader.readAsDataURL($event.target.files[0]);
      }
        let fileList: FileList = $event.target.files;  
        this.ktpName = fileList[0].name;
    }
  }

  doInputFile(element) {
    switch (element) {
      case 'foto':
        $('#input-avatar').click()
        break;
      case 'ktp':
        $('#input-ktp').click()
        break;
      default:
        break;
    }
  }

  navigateTo(page) {
    switch (page) {
      case 'login':
        this.router.navigateByUrl('login')
        break;
    
      default:
        break;
    }
  }

  openCalendar() {
    $('#cal-hide').click()
  }

  

  normalizeDate() {
    let date = this.formRegist.birth
    let dateChiper = date.split("T")
    let dateArr = dateChiper[0].split("-");
    let dateText;
    switch (dateArr[1]) {
      case "01":
        dateText = dateArr[2]+" Januari "+dateArr[0]
        break;
      case "02":
        dateText = dateArr[2]+" Februari "+dateArr[0]
        break;
      case "03":
        dateText = dateArr[2]+" Maret "+dateArr[0]
        break;
      case "04":
        dateText = dateArr[2]+" April "+dateArr[0]
        break;
      case "05":
        dateText = dateArr[2]+" Mei "+dateArr[0]
        break;
      case "06":
        dateText = dateArr[2]+" Juni "+dateArr[0]
        break;
      case "07":
        dateText = dateArr[2]+" Juli "+dateArr[0]
        break;
      case "08":
        dateText = dateArr[2]+" Agustus "+dateArr[0]
        break;
      case "09":
        dateText = dateArr[2]+" September "+dateArr[0]
        break;
      case "10":
        dateText = dateArr[2]+" Oktober "+dateArr[0]
        break;
      case "11":
        dateText = dateArr[2]+" November "+dateArr[0]
        break;
      case "12":
        dateText = dateArr[2]+" Desember "+dateArr[0]
        break;
      default:
        break;
    }
    this.bod = dateText;
  }

  checkRegist() {
    // this.formRegist.avatar = this.file
    let msgErr = '';
    let validations_form = this.formBuilder.group({
      name: new FormControl(this.formRegist.name, Validators.required),
      email: new FormControl(this.formRegist.email, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])),
      password: new FormControl(this.formRegist.password, Validators.compose([
        Validators.minLength(6)])),
    });
    if(!validations_form.controls.name.valid) {
      msgErr += "<small style='color:red'>* Nama tidak boleh kosong</small><br>"
      // this.validatorRegist.name.msg = "Nama tidak boleh kosong";
    }
    if(!validations_form.controls.email.valid) {
      msgErr += "<small style='color:red'>* Email tidak boleh kosong atau inputan tidak sesuai dengan format Email</small></br>";
      // this.validatorRegist.email.msg = "Email tidak boleh kosong atau inputan tidak sesuai dengan format Email";
    }
    if(!validations_form.controls.password.valid) {
      msgErr += "<small style='color:red'>* Password minimal 6 karakter</small></br>"
      // this.validatorRegist.password.msg = "Password minimal 6 karakter";
    }  
    if(validations_form.valid) {
      // this.doSubmit('signin')
      let form = {
        email: this.formRegist.email,
        name: this.formRegist.name,
        birth: this.formRegist.birth,
        address: this.formRegist.address,
        gender: this.formRegist.gender,
        avatar: this.formRegist.avatar,
        ktp: this.formRegist.ktp,
        password: this.formRegist.password,
        confirmPassword: this.formRegist.confirmPassword,
        role: this.formRegist.role,
        usia: this.formRegist.usia,
        agama: this.formRegist.agama,
        marital_status: this.formRegist.marital_status,
        birth_place: this.formRegist.birth_place,
        job: this.formRegist.job
      }
      // Swal.fire({
      //   title: "Hasil Capture",
      //   text: JSON.stringify(form)
      // })
      this.doRegist(form);
      console.log(form)
    } else {
      Swal.fire({
        title: 'Opss..',
        type: 'error',
        html: msgErr,
        showCloseButton: true,
        showCancelButton: true,
        focusConfirm: false,
        showConfirmButton: false,
        cancelButtonAriaLabel: 'Tutup',
      })
      console.log("Form = "+ false)
    }
    console.log(validations_form)
  }

  doRegist(form) {
    let timerInterval
    let formLogin = {
      email: this.formRegist.email,
      password: this.formRegist.password
    }
    const toastRegister = Swal.mixin({
      title: '<strong>Mohon Tunggu</strong>',
      text: 'Sedang dalam proses registrasi',
      timer: 3000,
    })
    let toastLogin;
    toastRegister.fire({
      onBeforeOpen: () => {
        // this.doLogin()
        toastRegister.showLoading();
        timerInterval = setInterval(() => {
        }, 3000)
      },
      onOpen: async () => {
        toastRegister.stopTimer();
        try {
          await this.apiUser.register(form).subscribe((resp: any) => {
            console.log(resp)
            if(resp && resp.code == 409)  {
              Swal.fire({
                type: 'error',
                title: 'Oops...',
                text: resp.msg
              })
              
            } else { 
              toastLogin = Swal.mixin({
                title: '<strong>Mohon Tunggu</strong>',
                text: 'Proses login aplikasi ...',
                timer: 3000,
              })
              toastLogin.fire({
                onBeforeOpen: () => {
                  // this.doLogin()
                  toastRegister.resumeTimer()
                  toastLogin.showLoading();
                  timerInterval = setInterval(() => {
                  }, 3000)
                },
                onOpen: async () => {
                  toastLogin.stopTimer();
                  let formSpecialist = [];
                  await this.apiUser.login(formLogin).subscribe(async (res :any) => {
                    let user = res;
                    if(res) {
                      if(res.id) {
                        await this.categories.forEach(category => {
                          if(category.checked) {
                            formSpecialist.push({
                              user_id: user.id,
                              category_id: category.id
                            })
                          }
                        });
                        console.log(formSpecialist)
                        await this.apiUser.addSpecialist(formSpecialist).subscribe(res => console.log(res))
                        let formWeekly = {
                          conselor_id: user.id
                        }
                        // await this.apiUser.postWeekly(formWeekly).subscribe(weekly => console.log(weekly))
                        await localStorage.setItem("_USER", JSON.stringify({_ID: user.id, role:user.role, status:user.status}));
                        await localStorage.setItem('_firstTime', "true")
                      }
                    } 
                  });
                  await setTimeout(() => {
                    window.location.href = 'home';
                  }, 3000)
                  toastLogin.resumeTimer();
                },
                onClose: () => {
                  clearInterval(timerInterval)
                  
                }
              })
            }
          });
          // Swal.resumeTimer();
        } catch (error) {
          alert(error)
        }
        
      },
      onClose: () => {
        clearInterval(timerInterval)
      },
    })
  }

  image: any;
  openCam(sourceType: PictureSourceType, imgFor: 'ktp' | 'pp'){

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: sourceType,
      mediaType: this.camera.MediaType.PICTURE
    }
    

    this.camera.getPicture(options).then((imageData) => {
     let base64Image = 'data:image/jpeg;base64,' + imageData;
     this.image=(<any>window).Ionic.WebView.convertFileSrc(imageData);
     this.startUpload(imageData, imgFor);
    }, (err) => {
     // Handle error
     alert("error "+JSON.stringify(err))
    });

  }

  async selectImage(imgFor: 'ktp' | 'pp') {
    const actionSheet = await this.actionSheetCtrl.create({
        header: "Select Image source",
        buttons: [{
                text: 'Load from Library',
                handler: () => {
                    this.openCam(this.camera.PictureSourceType.PHOTOLIBRARY, imgFor);
                }
            },
            {
                text: 'Use Camera',
                handler: () => {
                    this.openCam(this.camera.PictureSourceType.CAMERA, imgFor);
                }
            },
            {
                text: 'Cancel',
                role: 'cancel'
            }
        ]
    });
    await actionSheet.present();
}

  startUpload(imgEntry, imgFor: 'ktp' | 'pp') {
    // this.error = null;
    // this.loading = await this.loadingCtrl.create({
    //   message: 'Uploading...'
    // });

    // this.loading.present();
    let date = new Date()
    if(imgFor == 'ktp') {
      this.ktpName = 'ktp-'+date.getTime()+'.jpg';
    } else if(imgFor == 'pp') {
      this.fotoName = 'profile-'+date.getTime()+'.jpg'
    }

    window['resolveLocalFileSystemURL'](imgEntry,
      entry => {
        entry['file'](async (file: any) => {
         
          this.readFile(file, imgFor)
        });
      });
  }
  async readFile(file: any, imgFor: 'ktp' | 'pp') {
    let folder;
    if(imgFor == 'ktp') {
      // this.fileKtp = file
      // this.ktpName = file.name;
      folder = 'ktp'
      // if(this.idKtp) {
      //   let formDestroy = new FormData()
      //   formDestroy.append('public_id', this.idKtp)
      //   this.clodinary.destroy(formDestroy).subscribe(resp => resp)
      // }
    } else if(imgFor == 'pp') {
      // this.file = file
      // this.fotoName = await file.name
      folder = 'profile'
      // if(this.idPhoto) {
      //   let formDestroy = new FormData()
      //   formDestroy.append('public_id', this.idPhoto)
      //   this.clodinary.destroy(formDestroy).subscribe(resp => resp)
      // }
    }
    
      
    const reader = new FileReader();
    reader.onloadend = async () => {
      Swal.fire({
        title: 'Mohon tunggu',
        text: 'upload file',
        timer: 3000,
        onBeforeOpen:async  () => {
          Swal.showLoading();
          this.fotoName = await file.name
          folder = 'profile'
        },
        onOpen: async () => {
          Swal.stopTimer();
          this.fotoName = await file.name
          const formData = new FormData();
          const imgBlob = new Blob([reader.result], {type: file.type});
          formData.append('file', imgBlob, file.name);
          formData.append('folder', folder)
          formData.append('upload_preset', 'c2y6kxd3')
          await this.clodinary.upload(formData)
          .subscribe( async (resp: any) => {
            this.urlPhoto = resp.url
            this.idPhoto = resp.public_id
            this.formRegist.avatar = resp.url
            
          })
          await setTimeout(() => {
            Swal.resumeTimer()
          }, 3000)
        }
      })
      
      
      // this.postData(formData);
      
    };
    await reader.readAsArrayBuffer(file);
  }

  async openForm() {
    const modal = await this.modalCtrl.create({
      component: ConselorFormPage,
      componentProps: {
        categories: this.categories,
      }
    });
    modal.onDidDismiss().then(res => {
      if(res.data) {
        console.log(res)
        this.categories = res.data.categories
        this.formRegist.role = res.data.role
        this.checkRegist()
      }
      // this.router.navigateByUrl('home')
    })
    return await modal.present();
  }
}
