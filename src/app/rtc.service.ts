import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ProvidersService } from './services/providers.service';
import { async } from 'q';

declare var apiRTC;
declare var apiCC;
const STATE_WAIT = "wait";
const STATE_ESTABLISH = "establish";
const STATE_INCALL = "incall";


@Injectable({
  providedIn: 'root'
})
export class RTCService {
  state;
  userAgent: any;
  webRTCClient: any;
  showStatus: boolean;
  showMyVideo: boolean;
  callType;
  session;

  incomingCall;
  incomingCallId;
  onstate;

  userInteraction = {
    callee: {},
    caller: {}
  }

  constructor(private router: Router, private api: ProvidersService) {
    this.incomingCallHandler = this.incomingCallHandler.bind(this);
    this.userMediaErrorHandler = this.userMediaErrorHandler.bind(this);
    this.remoteStreamAddedHandler = this.remoteStreamAddedHandler.bind(this);
    this.hangupHandler = this.hangupHandler.bind(this);
    this.sessionReadyHandler = this.sessionReadyHandler.bind(this);
    this.userMediaSuccessHandler = this.userMediaSuccessHandler.bind(this);
    this.InitializeApiRTC()
   }


  async InitializeApiRTC() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.userAgent = await new apiRTC.UserAgent({
      uri: 'apzkey:c83c92d8ec6bccd086e097ed028ea672'
    });
      await this.userAgent.register({id: id}).then(async (session: any) => {
        // ok
        this.session = session
        await this.sessionReadyHandler(session)
        console.log("Registration OK");
        console.log('Session created with sessionId' + session.id);
        this.state = STATE_WAIT;
    }).catch(function (error) {
        // error
        console.log("Registration error");
    });
  }

  
  getSession() {
    return this.userAgent
  }

  getUser() {
    return this.userInteraction
  }

  /**
   * Call Action
   */
  async pushCallAudio( distantNumber) {
        this.onstate = "call"
        this.callType = "audio"
        console.log("Push, callState="+this.state);
        if(distantNumber && this.state == STATE_WAIT) {
          this.webRTCClient.callAudio(distantNumber);
        } else if(this.state == STATE_INCALL) {
          this.state = STATE_WAIT;
          this.webRTCClient.hangUp();
        }

  }
  async pushCall(distantNumber) {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.onstate = "call"
        this.callType = "audio"
        console.log("Push, callState="+this.state);
        if(distantNumber && this.state == STATE_WAIT) {
          this.webRTCClient.call(distantNumber);
        } else if(this.state == STATE_INCALL) {
          this.state = STATE_WAIT;
          this.webRTCClient.hangUp();
        }
    
  }

  sessionReadyHandler(e) {
    console.log("sessionReadyHandler", e);
    apiRTC.addEventListener("incomingCall", this.incomingCallHandler);
    apiRTC.addEventListener("userMediaError", this.userMediaErrorHandler);
    apiRTC.addEventListener("remoteStreamAdded", this.remoteStreamAddedHandler);
    apiRTC.addEventListener("userMediaSuccess", this.userMediaSuccessHandler);
    apiRTC.addEventListener("callEstablished", this.callEstablishedHandler);
    apiRTC.addEventListener("hangup", this.hangupHandler);
    this.webRTCClient = apiCC.session.createWebRTCClient({});
    this.webRTCClient.setUserAcceptOnIncomingCall(true)
    this.webRTCClient.setUserAcceptOnIncomingCallBeforeGetUserMedia(true)
    // this.infoLabel = "Your local ID : "+apiCC.session.apiCCId;
  }

  callEstablishedHandler(e) {
    console.log("callEstablishedHandler", e)
    // this.onstate = "establish"
    this.onstate = 'accept'
  }

  incomingCallHandler(e) {
    console.log("incomingCallHandler", e);
    
    this.state = STATE_INCALL;
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.onstate = "incoming"
          this.incomingCall = e.detail.callId
          this.incomingCallId = e.detail.callerId
          if(e.detail.destCallType == 'media') {
            this.router.navigateByUrl("vidcall")
            console.log("media")
          } else {
            console.log("call")
            this.router.navigateByUrl("caller")
          }
    // if(!this.incomingCall) {
    //   this.api.getProfile(id)
    //   .subscribe(async (caller: any) => {
    //     this.userInteraction.caller = caller.data[0]
    //     this.api.getProfile(e.detail.callerId)
    //     .subscribe(async (callee: any) => {
    //       this.userInteraction.callee = callee.data[0]
          
          
    //     })
    //   })
    // } 
    // this.buttonColor = COLOR_HANGOUT;
    // this.buttonLabel = LABEL_HANGOUT;
    // setTimeout(this.refreshVideoView,2000);
  }

  acceptCall() {
    console.log(this.incomingCall)
    this.onstate = 'accept'
    this.webRTCClient.acceptCall(this.incomingCall)
  }

  refuse() {
    this.webRTCClient.refuseCall(this.incomingCall)
    this.incomingCall = false
    window.location.href = '/home'
  }

  decline() {
    this.webRTCClient.hangUp();
    window.location.href = '/home'
  }

  hangupHandler(e) {
    console.log("hangupHandler");
    this.state = STATE_WAIT;
    this.incomingCall = false
    // this.buttonColor = COLOR_CALL;
    // this.buttonLabel = LABEL_CALL;
    this.initMediaElementState(e.detail.callId);
    window.history.back();
  }

  userMediaSuccessHandler(e) {
    console.log("userMediaSuccessHandler",e);
    this.webRTCClient.addStreamInDiv(
      e.detail.stream,
      e.detail.callType,
      "mini",
      'miniElt-' + e.detail.callId,
      {width : "128px", height : "96px"},
      true
    );
  }

  userMediaErrorHandler(e) {
  }

  remoteStreamAddedHandler(e) {
    console.log("remoteStreamAddedHandler",e);
    this.state = STATE_INCALL;
    this.onstate = 'accept'
    // this.buttonColor = COLOR_HANGOUT;
    // this.buttonLabel = LABEL_HANGOUT;
    this.webRTCClient.addStreamInDiv(
      e.detail.stream,
      e.detail.callType,
      "remote",
      'remoteElt-' + e.detail.callId,
      {},
      false
    );
    // setTimeout(this.refreshVideoView,1000);
  }

  initMediaElementState(callId) {
    this.webRTCClient.removeElementFromDiv('mini', 'miniElt-' + callId);
    this.webRTCClient.removeElementFromDiv('remote', 'remoteElt-' + callId);
  }

  // toogleMute() {
  //   this.webRTCClient.toggleAudioMute(this.incomingCall)
  // }
  // toggleVideoMute() {
  //   this.webRTCClient.toggleVideoMute(this.incomingCall)
  // }
}
