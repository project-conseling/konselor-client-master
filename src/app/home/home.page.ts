import { Component } from '@angular/core';
import { ProvidersService } from '../services/providers.service';
import { BackendSocketService } from '../backend-socket.service';
import { RTCService } from '../rtc.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Socket } from 'ng-socket-io';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  segmentValue = "schedule"
  role = 1;
  complaintTotal = 0;
  conselingsDay = [];

  constructor(public socket: Socket, public api: ProvidersService, private socketService: BackendSocketService, public rtcService: RTCService, private router: Router) {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID;
    
    this.api.getMySchedule(id)
    .subscribe((res: any) => {
      console.log(res)
      this.getUpdateComplaint().subscribe(resp => {
        if(this.role == 9) {
          this.complaintTotal ++;
        }
      })
      // if(res.data.length > 0) {
      //   res.data.forEach(element => {
      //     element.time.forEach(time => {
      //       if(time.status == 1) {
      //         this.conselingsDay.push(
      //           {
      //             date: new Date(element.date),
      //             subTitle:"Konseling",
      //             marked: true
      //           }
      //         )
      //       }
      //     });
      //   });
      // }
    })

    this.role = JSON.parse(storeLocal).role
    this.socketService.statusActive()
    
    
    if(this.role == 9) {
      this.api.getComplain()
      .subscribe((res: any) => {
        res.forEach(element => {
          if(element.status != 9) {
            this.complaintTotal ++;
          }
        });
      })
    }
  }

  controlConseling(segment) {
    console.log(segment)
    this.segmentValue = segment;
    
  }
  getUpdateComplaint() {
    let observable = new Observable(obs => {
      this.socket.on('update-complaint', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }

  

  segmentChanged($event) {
    this.segmentValue = $event.detail.value;
  }
  logoutConfirm() {
    this.router.navigateByUrl('logout')
  }
}
