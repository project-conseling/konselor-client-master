import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomePage } from './home.page';
import { ListAppliedPage } from '../complainment/list-applied/list-applied.page';
import { ChatPage } from '../chat/chat.page';
import { SchedulePage } from '../schedule/schedule.page';
import { ListConselingPage } from '../conseling/list-conseling/list-conseling.page';
import { ConselingResultPage } from '../conseling/conseling-result/conseling-result.page';
import { DetailComplaintPage } from '../complainment/detail-complaint/detail-complaint.page';
import { ConselorSelectionPage } from '../complainment/conselor-selection/conselor-selection.page';
import { NoteComplaintPage } from '../complainment/note-complaint/note-complaint.page';
import { SetSchedulePage } from '../schedule/set-schedule/set-schedule.page';
import { CalendarModule } from 'ion2-calendar';
import { LogChatPage } from '../conseling/log-chat/log-chat.page';

@NgModule({
  entryComponents:[
    ListAppliedPage,
    ChatPage,
    SchedulePage,
    ListConselingPage,
    ConselingResultPage,
    DetailComplaintPage,
    ConselorSelectionPage,
    NoteComplaintPage,
    SetSchedulePage,
    LogChatPage,
  ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CalendarModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  declarations: [
    HomePage,
    LogChatPage,
    ListAppliedPage,
    ChatPage,
    SchedulePage,
    ListConselingPage,
    ConselingResultPage,
    DetailComplaintPage,
    ConselorSelectionPage,
    NoteComplaintPage,
    SetSchedulePage
  ]
})
export class HomePageModule {}
