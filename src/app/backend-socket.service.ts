import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Socket } from 'ng-socket-io';

@Injectable({
  providedIn: 'root'
})
export class BackendSocketService {
  activeId = []
  constructor(private socket: Socket) { }
  statusActive() {
    let storeLocal = localStorage.getItem('_USER');
    let id = JSON.parse(storeLocal)._ID
    this.socket.emit('set-nickname', id)
    setTimeout(() => {
      this.statusActive()
    }, 3000)
  }

  getUpdate(){
    let observable = new Observable(obs => {
      this.socket.on('status-changed', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }

  getMessages() {
    let observable = new Observable(obs => {
      this.socket.on('message', data => {
        obs.next(data)
      }) 
    })
    return observable;
  }

  getActiveUser() {
    let observable = new Observable(obs => {
      this.socket.on('users-changed', data => {
        // console.log(data)
        obs.next(data)
      }) 
    })
    return observable;
  }
}
