import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule', canActivate: [AuthGuard]},
  { path: 'home/:slug', loadChildren: './home/home.module#HomePageModule', canActivate: [AuthGuard]},
  { path: 'register', loadChildren: './user/registrasi/registrasi.module#RegistrasiPageModule' },
  { path: 'login', loadChildren: './user/login/login.module#LoginPageModule' },
  { path: 'logout', loadChildren: './user/logout/logout.module#LogoutPageModule' },
  { path: 'forget-password', loadChildren: './user/forget-password/forget-password.module#ForgetPasswordPageModule' },
  { path: 'room-chat/:id', loadChildren: './chat/room-chat/room-chat.module#RoomChatPageModule' },
  { path: 'caller', loadChildren: './chat/caller/caller.module#CallerPageModule' },
  { path: 'vidcall', loadChildren: './chat/vidcall/vidcall.module#VidcallPageModule' },
  { path: 'cs', loadChildren: './chat/cs/cs.module#CsPageModule' },
  { path: 'form-answer', loadChildren: './chat/form-answer/form-answer.module#FormAnswerPageModule' },
  { path: 'forget', loadChildren: './user/reset-password/reset-password.module#ResetPasswordPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
